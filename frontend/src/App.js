import React from 'react';

import {AppBar, Button, CssBaseline, makeStyles, Toolbar, Typography} from "@material-ui/core";
import TaskPanel from "./features/Tasks/TaskPanel";
import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import SignIn from "./features/signInUp/SignIn";
import SignUp from "./features/signInUp/SignUp";
import {useDispatch, useSelector} from "react-redux";
import {changeLoggedInStatus, removeAll} from "./features/Tasks/TasksSlice";


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));


function App() {
    const classes = useStyles();
    const loggedIn = useSelector(state => state.Tasks.loggedIn)
    const dispatch = useDispatch()

    const onClick = () =>{
        dispatch(removeAll())
        dispatch(changeLoggedInStatus(false))
        fetch('todos/logout')
    }

    return (
        <Router>
            <CssBaseline/>
            <AppBar position="static">
                <Toolbar>

                    <Typography variant="h6" className={classes.title}>
                        ToDo
                    </Typography>
                    {loggedIn ?
                        <Button color="inherit" onClick={onClick}>
                            <Link to="/login"
                                  style={{textDecoration: 'none', color: 'white'}}>Logout</Link>
                        </Button> : <></>}
                </Toolbar>
            </AppBar>
            <Switch>
                <Route path="/login">
                    <SignIn/>
                </Route>
                <Route path="/signup">
                    <SignUp/>
                </Route>
                <Route path="/">
                    <TaskPanel/>
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
