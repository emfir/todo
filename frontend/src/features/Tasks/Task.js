import React, {useState} from 'react'
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {AccordionDetails, Checkbox, TextField} from "@material-ui/core";
import Accordion from "@material-ui/core/Accordion";
import {updateTask, updateTodo} from "./TasksSlice";
import {useDispatch} from "react-redux";
import Typography from "@material-ui/core/Typography";

const Task = ({info}) => {
    const dispatch = useDispatch();
    const [checked, changeChecked] = useState(info.completed)

    const onBlur = (event) => {
        if (event.target.value !== info.title) {
            // dispatch(updateTask({ id: info.id, changes: { title: event.target.value } }))
              dispatch(updateTodo({id: info.id, title: event.target.value, completed: checked}))
        }
    }

    const handleChecked = () => {
        const newValue = !checked
        changeChecked(newValue);
        dispatch(updateTodo({id: info.id, title: info.title, completed: newValue}));
    }

    return (
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon/>}
                aria-label="Expand"
                aria-controls="additional-actions1-content"
                id="additional-actions1-header"
            >

                <Checkbox
                    checked={checked}
                    onChange={handleChecked}
                    inputProps={{'aria-label': 'primary checkbox'}}
                />
                <TextField
                    id="standard-full-width"

                    style={{margin: 8}}
                    defaultValue={info.title}
                    fullWidth
                    margin="normal"
                    onBlur={onBlur}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </AccordionSummary>
            <AccordionDetails>
                <Typography color="textSecondary">
                    created: {(new Date(info.created)).toDateString()}
                </Typography>
            </AccordionDetails>
        </Accordion>
    )
}

export default Task;