import React, {useState} from 'react';
import {Button, Container} from "@material-ui/core";
import NewTask from "./NewTask";
import TodoList from "./TodoList";


const TaskPanel = () => {
    const [displayDone, setDisplayDone] = useState(false)

    let doneList;

    if (displayDone) {
        doneList = <TodoList done={true}/>;
    } else {
        doneList = <></>
    }

    const onClick = () => {
        setDisplayDone(!displayDone)
    }


    return <Container maxWidth="sm">

        <NewTask/>
        <TodoList done={false}/>
        <div style={{display: "flex", justifyContent: "center"}}>
            <Button variant="contained" color="primary" onClick={onClick} style={{margin: '1rem'}}>
                Done
            </Button>
        </div>
        {doneList}
    </Container>


}


export default TaskPanel