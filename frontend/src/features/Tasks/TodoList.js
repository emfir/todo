import React, {useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {getTodos} from "./TasksSlice";
import {makeStyles} from '@material-ui/core/styles';
import Task from "./Task";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
});


const TodoList = ({done}) => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const todos = useSelector(e => Object.values(e.Tasks.entities).filter(en => en.completed === done).reverse())
    const status = useSelector(e => e.Tasks.status)
    const loggedIn = useSelector(store => store.Tasks.loggedIn);
    let history = useHistory();

    useEffect(() => {
        if (status === 'idle') {
            dispatch(getTodos())
        }
    }, [dispatch, status])

    if (loggedIn === false) {
        history.push("/login");
    }


    return (
        <div className={classes.root} style={{alignContent: 'center'}}>
            {todos.map(e => <Task info={e} key={e.id}/>)}

        </div>
    );
}


export default TodoList;
