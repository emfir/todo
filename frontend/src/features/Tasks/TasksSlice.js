import {createAsyncThunk, createEntityAdapter, createSlice} from '@reduxjs/toolkit'
import Cookies from 'js-cookie'
import "regenerator-runtime/runtime.js";

let csrftoken = Cookies.get('csrftoken');

export const getTodos = createAsyncThunk('/Tasks/getTodos',
    async () => {
        const response = await fetch("/todos/task/")
        const status = response.status;
        const data = await response.json();
        return {status: status, data: data};
    }
)

export const addTodo = createAsyncThunk('/Tasks/addTodo', async (newTodo) => {
        return await fetch("/todos/task/", {
            method: 'POST',
            headers: {
                'X-CSRFToken': csrftoken,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                    title: newTodo
                }
            )
        }).then(response => response.json())
    }
)
export const updateTodo = createAsyncThunk('/Tasks/updateTodo', updatedTodo => fetch(`/todos/task/${updatedTodo.id}/`, {
        method: 'PUT',
        headers: {
            'X-CSRFToken': csrftoken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
                title: updatedTodo.title,
                completed: updatedTodo.completed
            }
        )
    }).then(response => response.json())
)


// export const addNewTask = createAsyncThunk('/Task/addTask');


const tasksAdapter = createEntityAdapter({
    selectId(model) {
        return model.id;
    }
})


const initialState = tasksAdapter.getInitialState({
    loggedIn: null,
    status: "idle",
    error: null
});

export const TasksSlice = createSlice({
    name: "Tasks",
    initialState,
    reducers: {
        removeAll(state,action){
            tasksAdapter.removeAll(state);
            state.status = "idle";
        },
        changeLoggedInStatus(state, action){
            state.loggedIn = action.payload
            csrftoken = Cookies.get('csrftoken');
        },
        changeTaskStatus: {
            reducer(state, action) {
                const id = action.payload;
                const obj = state.ids.find(e => e === id)
                state.entities[obj].completed = !state.entities[obj].completed
            }
        },
        updateTask: tasksAdapter.updateOne,
        addNewTask: {
            reducer: tasksAdapter.addOne,
            prepare: (tittle) => {
                return {
                    payload: {
                        id: nanoid(),
                        title: tittle,
                        userId: 1,
                        completed: false
                    }
                }
            }
        }


    },
    extraReducers: {
        [getTodos.fulfilled]:
            (state, action) => {
                if (action.payload.status === 403) {
                    state.loggedIn = false;

                } else if (action.payload.status === 200) {
                    tasksAdapter.upsertMany(state, action.payload.data)
                    state.status = 'fulfilled';
                    state.loggedIn = true;
                }
            }
        ,
        [addTodo.fulfilled]:
        tasksAdapter.addOne,
        [updateTodo.fulfilled]:
            (state, action) => {
                tasksAdapter.updateOne(state, {id: action.payload.id, changes: {...action.payload}})
            }
    }
})

export const {changeTaskStatus, addNewTask, updateTask, changeLoggedInStatus, removeAll} = TasksSlice.actions;
export default TasksSlice.reducer;