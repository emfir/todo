import {Button, makeStyles, TextField} from "@material-ui/core";
import React, {useState} from 'react';
import {addTodo} from "./TasksSlice";
import {useDispatch} from "react-redux";


const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridTemplateColumns: 'repeat(12, 1fr)',
        gridTemplateRows: '1rem auto 1rem'
    }
}));

const NewTask = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [task, taskSetter] = useState('')

    const onInputChange = (event) => taskSetter(event.target.value);

    const onClick = () => {
        if (task) {
            dispatch(addTodo(task))
            taskSetter('')
        }
    }

    const onKeyUp = (e) => {
        if (e.key === 'Enter') {
            onClick();
        }

    }


    return <div>
        <div className={classes.container}>

            <div style={{gridColumnEnd: 'span 10', gridRow: '2/span 1'}}>


                <TextField
                    id="outlined-helperText"
                    label="New task"
                    // defaultValue="Default Value"
                    value={task}
                    variant="outlined"
                    fullWidth
                    onChange={onInputChange}
                    onKeyUp={(onKeyUp)}

                />
            </div>
            <div style={{gridColumn: '11/span 2', gridRow: '2/span 1'}}>
                <Button variant="contained" color="primary" fullWidth style={{height: '100%'}} onClick={onClick}>
                    Add
                </Button>
            </div>
        </div>
    </div>
}

export default NewTask;