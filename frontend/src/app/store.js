import { configureStore } from '@reduxjs/toolkit';
import TasksReducer from '../features/Tasks/TasksSlice'



export default configureStore({
  reducer: {
    Tasks: TasksReducer
  },
});
