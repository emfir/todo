from rest_framework import serializers

from .models import Task, User


class TaskSerializer(serializers.ModelSerializer):
    completed = serializers.BooleanField(default=False, required=False)

    class Meta:
        model = Task
        fields = ['id','title', 'completed', 'created']


class UserLogInSerializer(serializers.ModelSerializer):
    password = serializers.CharField(min_length=3)
    username = serializers.CharField()

    class Meta:
        model = User
        fields = ('username', 'password')


class UserSerializer(serializers.ModelSerializer):
    """
    Currently unused in preference of the below.
    """
    email = serializers.EmailField(
        required=True
    )
    username = serializers.CharField()
    password = serializers.CharField(min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ('email', 'username', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)  # as long as the fields are the same, we can just use this
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance
