from django.contrib.auth import authenticate, login, logout
from rest_framework import viewsets, permissions, status
# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Task
from .permisions import IsOwner
from .serializers import TaskSerializer, UserSerializer, UserLogInSerializer


# Create your views here.
class TaskViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated,
                          IsOwner]
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


def logout_view(request):
    logout(request)


class UserCreate(APIView):

    def post(self, request, format='json'):
        serializer = UserSerializer(data=request.data)

        if serializer.is_valid():
            user = serializer.save()
            if user:
                json = serializer.data
                return Response(json, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserLogIn(APIView):

    def post(self, request, format='json'):
        serializer = UserLogInSerializer(data=request.data)

        if serializer.is_valid():
            username = serializer.data.get('username')
            password = serializer.data.get('password')
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return Response(status=status.HTTP_200_OK)
            else:
                return Response()
        else:
            print("someone tried to login and failed!")
            print("Username: {} and password {}".format(username, password))
            return Response(status=status.HTTP_400_BAD_REQUEST)
