from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from .views import TaskViewSet, UserCreate, UserLogIn, logout_view

router = DefaultRouter()
router.register(r'task', TaskViewSet)

urlpatterns = [
    path('login', UserLogIn.as_view()),
    path('logout', logout_view),
    path('register', UserCreate.as_view()),
    path('', include(router.urls)),


]

